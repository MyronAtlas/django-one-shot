from django.urls import path
from todos.views import show_list, create_list, list_detail, edit_list, delete_model_name, create_item, edit_item

urlpatterns = [
    path("",show_list, name = "todo_list_list"),
    path("create/", create_list, name = "todo_list_create"),
    path("<int:id>/", list_detail, name = "todo_list_detail"),
    path("<int:id>/edit/", edit_list, name = "todo_list_update"),
    path("<int:id>/delete/", delete_model_name, name = "todo_list_delete"),
    path("items/create/", create_item, name= "todo_item_create"),
    path("items/<int:id>/edit/", edit_item, name = "todo_item_update")

]
