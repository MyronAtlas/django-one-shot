from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.
def show_list(request):
    showlist = TodoList.objects.all()
    context = {
        "todos": showlist
    }
    return render(request, "todos/list.html", context )

def list_detail(request,id):
    showdetail = get_object_or_404(TodoList,id=id)
    context = {
        "list_detail":showdetail
    }
    return render(request, "todos/details.html", context)

def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()
        context = {
            "form":form,
        }
        return render(request, "todos/create.html", context)

def edit_list(request,id):
    editlist= get_object_or_404(TodoList,id=id)
    if request.method == "POST":
        form=TodoListForm(request.POST, instance = editlist)
        if form.is_valid():
           list = form.save()
           return redirect("todo_list_detail", list.id)
    else:
        form = TodoListForm(instance=editlist)
        context = {
            "form": form
        }
        return render(request, "todos/edit.html", context)

def delete_model_name(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")
    
    return render(request, "todos/delete.html")

def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", list.id)
    else:
        form = TodoItemForm()
        context = {
            "form":form
        }
        return render(request,"todos/item.html", context)

def edit_item(request,id):
    edititem = get_object_or_404(TodoItem,id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=edititem)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.id)
    else:
        form = TodoItemForm(instance=edititem)
        context = {
            "form":form
        }
        return render(request, "todos/edititem.html", context)
